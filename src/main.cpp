#include <iostream>

#include "Game.h"

int main()
{
	Game::initialize();
	Game::display();
	Game::assignValues();
	
	bool running = true;
	while (running)
	{
		Game::display();		
		bool found = Game::checkResult();
		
		if (!found)
		{
			Game::executeTurn();
		}
		else
		{
			running = false;
		}
	}
	
	return 0;
}