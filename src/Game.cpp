#include "Game.h"

#include <memory>
#include <array>
#include <algorithm>
#include <iostream>
#include <functional>
#include <vector>

#define INDEX(X, Y) (X * COLUMNS + Y)

namespace Game
{
	const std::array<char, 3> CHARACTERS = {'-', 'X', 'O'};
	
	enum Value
	{
		VALUE_NONE = 0,
		VALUE_X = 1,
		VALUE_O = 2
	};
	
	typedef std::array<Value, SIZE> Board;
		
	struct Result
	{
		Value winner;
		bool gameover;
	};
	
	std::function<void()> currentTurn;
	std::function<void()> nextTurn;
	
	namespace Ai
	{
		struct Node
		{
			Board state;
			std::vector<Node> children;
			int value;
		};	
	}
}

namespace Game
{
	static Board board;
	static Value playerValue;
	static Value computerValue;
	
	static Result checkBoard(const Board& board)
	{
		Result result;
		result.winner = VALUE_NONE;
		result.gameover = false;
	
		Value value = VALUE_NONE;
		bool found = false;
		// Check horizontals
		for (int i = 0; i < ROWS; ++i)
		{
			value = board[INDEX(i, 0)];
			if (value != VALUE_NONE)
			{
				found = true;
				for (int j = 1; j < COLUMNS; ++j)
				{
					found = found && board[INDEX(i, j)] == value;
				}
				
				if (found)
				{
					result.winner = board[INDEX(i, 0)];
					result.gameover = true;
					return result;
				}
			}
		}
		
		// Check verticals
		for (int i = 0; i < COLUMNS; ++i)
		{
			value = board[INDEX(0, i)];
			if (value != VALUE_NONE)
			{
				found = true;
				for (int j = 1; j < ROWS; ++j)
				{
					found = found && board[INDEX(j, i)] == value;
				}
				
				if (found)
				{
					result.winner = board[INDEX(0, i)];
					result.gameover = true;
					return result;
				}
			}
		}
	
		// Check diagonal one
		value = board[INDEX(0, 0)];
		found = true;
		if (value != VALUE_NONE)
		{
			for (int i = 1; i < ROWS; ++i)
			{
				found = found && board[INDEX(i, i)] == value;
			}
			
			if (found)
			{
				result.winner = board[INDEX(0, 0)];
				result.gameover = true;
				return result;
			}
		}	
		
		// Check diagonal two
		value = board[INDEX(0, COLUMNS - 1)];
		found = true;
		if (value != VALUE_NONE)
		{
			for (int i = 1; i < ROWS; ++i)
			{
				found = found && board[INDEX(i, COLUMNS - i - 1)] == value;
			}
			
			if (found)
			{
				result.winner = board[INDEX(0, COLUMNS - 1)];
				result.gameover = true;
				return result;
			}
		}
		
		// Check if draw
		bool moveAvailable = false;
		for (int i = 0; i < SIZE; i++)
		{
			if (board[i] == VALUE_NONE)
			{
				moveAvailable = true;
				break;
			}
		}
		
		result.gameover = !moveAvailable;
		return result;
	}
	
	void initialize()
	{
		std::fill(std::begin(board), std::end(board), VALUE_NONE);
	}
		
	void display()
	{
		std::system("clear");
		std::cout << "Tic Tac Toe" << std::endl;
		for (int i = 0; i < ROWS; ++i)
		{
			for (int j = 0; j < COLUMNS; ++j)
			{
				char c = CHARACTERS[board[INDEX(i, j)]];
				std::cout << c << " ";
			}
			std::cout << std::endl;
		}
	}
	
	static void doPlayerTurn()
	{
		int index;
		bool valid = false;
		
		do
		{
			std::cout << "Where do you want to place an " << CHARACTERS[playerValue] << "? (1 - 9): ";
			std::cin >> index;
			
			valid = index >= 1 && index <= SIZE && board[index - 1] == VALUE_NONE;
			
			if (valid == false)
			{
				std::cout << "Invalid Move! Please try again!" << std::endl;
			}
		}
		while (valid == false);
		
		board[index - 1] = playerValue;
	}
	
	static void printNode(Ai::Node& n)
	{
		for (auto& i : n.state)
		{
			std::cout << CHARACTERS[i] << " ";
		}
		std::cout << "Value: " << n.value << std::endl;
	}
	
	static int minimax(Ai::Node& root, Value turn, int depth)
	{	
		Result result = checkBoard(root.state);
		//std::cout << "Depth: " << depth << std::endl;
		if (result.gameover == true || depth == 0)
		{
			if (result.gameover == true)
			{
				if (result.winner == computerValue)
				{
					return 1;
				}
				else if (result.winner == playerValue)
				{
					return -1;
				}
				else
				{
					return 0;
				}
			}
			return 0;
		}
		else
		{
			std::array<bool, SIZE> visited;
			std::function<int(int, int)> comparator;
			int bestValue;
			Value nextTurn;
			
			std::fill(std::begin(visited), std::end(visited), false);
			
			if (turn == computerValue)
			{
				comparator = [](int one, int two) -> int {return one > two ? one : two;};
				nextTurn = playerValue;
				bestValue = -1;
			}
			else
			{
				comparator = [](int one, int two) -> int {return one < two ? one : two;};
				nextTurn = computerValue;
				bestValue = 1;
			}
			
			for (int i = 0; i < SIZE; ++i)
			{
				if (visited[i] == false && root.state[i] == VALUE_NONE)
				{
					Ai::Node child;
					std::copy(std::begin(root.state), std::end(root.state), std::begin(child.state));
					
					child.state[i] = turn;
					
					child.value = minimax(child, nextTurn, depth - 1);
					bestValue = comparator(child.value, bestValue);
					
					//printNode(child);
					
					root.children.push_back(child);
				}
				visited[i] = true;
			}
			
			return bestValue;
		}
	}
	
	static void decideComputerMove()
	{
		Ai::Node root;
		std::copy(std::begin(board), std::end(board), std::begin(root.state));
		
		// Build tree of depth n
		minimax(root, computerValue, -1);
				
		// Select winning move
		for (auto& child : root.children)
		{
			//printNode(child);
			if (child.value == 1)
			{
				board = child.state;
				return;
			}
		}
		
		// Select drawing move
		for (auto& child : root.children)
		{
			if (child.value == 0)
			{
				board = child.state;
				return;
			}
		}
		
		// Select any move
		if (root.children.size() > 0)
		{
			board = root.children[0].state;
		}
	}
	
	static void doComputerTurn()
	{
		decideComputerMove();
	}
	
	void assignValues()
	{
		char choice;
		
		do
		{
			std::cout << "Choose X or O: ";
			std::cin >> choice;
		}
		while (choice != 'X' && choice != 'O');
		
		if (choice == 'X')
		{
			playerValue = VALUE_X;
			computerValue = VALUE_O;
			
			currentTurn = doPlayerTurn;
			nextTurn = doComputerTurn;
		}
		else
		{
			playerValue = VALUE_O;
			computerValue = VALUE_X;
			
			currentTurn = doComputerTurn;
			nextTurn = doPlayerTurn;
		}
	}
	
	void executeTurn()
	{
		currentTurn();
		std::swap(currentTurn, nextTurn);
	}
	
	bool checkResult()
	{
		Result result = checkBoard(board);
		if (result.gameover)
		{
			switch (result.winner)
			{
				case VALUE_NONE:
				{
					std::cout << "Game Draw! Thank you for playing!" << std::endl;
					break;
				}
				
				default:
				{
					if (result.winner == playerValue)
					{
						std::cout << "Congratulations! You Win! Thank you for playing!" << std::endl;
					}
					else
					{
						std::cout << "Sorry! You Lost! Thank you for playing!" << std::endl;
					}
					break;
				}
			}
			return true;
		}
		return false;
	}
}