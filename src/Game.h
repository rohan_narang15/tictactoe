#ifndef _GAME_H
#define _GAME_H

#include <array>

namespace Game
{
	const int ROWS = 3;
	const int COLUMNS = 3;
	const int SIZE = ROWS * COLUMNS;
	
	void initialize();
	void assignValues();
	void display();
	void executeTurn();	
	bool checkResult();
}

#endif