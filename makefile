all: bin obj Game.o main.o
	clang++ -std=c++1y obj/Game.o obj/main.o -o bin/tictactoe
	
Game.o: obj
	clang++ -c -std=c++1y src/Game.cpp -o Game.o
	mv Game.o obj/Game.o
	
main.o: obj
	clang++ -c -std=c++1y src/main.cpp -o main.o
	mv main.o obj/main.o
	
clean:
	rm -f obj/*
	rm -f bin/*
	
bin:
	mkdir -p bin
	
obj:
	mkdir -p obj

